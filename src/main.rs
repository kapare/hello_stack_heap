use std::mem;

struct Point {
    x: f64,
    y: f64,
}

fn origin() -> Point {
    Point { x: 0.0, y: 0.0 }
}

fn fn_loop() {
    let mut idx: u8 = 0;
    println!("");
    println!("fn_loop:");
    loop {
        println!("loop# {}", idx);
        idx += 1;
        if idx >= 5 {
            break;
        }
    }
}

fn fn_while() {
    let mut idx: u8 = 0;
    println!("");
    println!("fn_while:");
    while idx <= 5 {
        println!("while# {}", idx);
        idx += 1;
    }
}

fn for_loop() {
    println!("");
    println!("for_loop:");
    for (pos, y) in (30..41).enumerate() {
        println!("for# {}:{}", pos, y);
    }
}

fn main() {
    println!("");
    println!("hello_stack_heap:");

    let p1 = origin();
    let p2 = Box::new(origin());

    println!("p1.x {} p1.y {}", p1.x, p1.y);
    println!("p2.x {} p2.y {}", p2.x, p2.y);
    println!("p1: {} bytes", mem::size_of_val(&p1));
    println!("p2: {} bytes", mem::size_of_val(&p2));

    let mut p3 = *p2;
    println!("p3.x {} p3.y {}", p3.x, p3.y);
    println!("p3: {} bytes", mem::size_of_val(&p3));
    p3.x = 3.3;
    p3.y = 3.3;

    let condition_point = if p3.x == 3.3 && p3.y == 3.3 {
        Point { x: 3.4, y: 3.4 }
    } else {
        Point { x: 3.0, y: 3.0 }
    };
    println!(
        "condition_point.x {} condition_point.y {}",
        condition_point.x, condition_point.y
    );
    println!(
        "is it 3.4 ? {}",
        if condition_point.x == 3.4 && condition_point.y == 3.4 {
            "YES"
        } else {
            "NO"
        }
    );

    fn_loop();
    fn_while();
    for_loop();
}
